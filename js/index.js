$(function () {
    $("[data-toggle='tooltip']").tooltip();

    $('[data-toggle="popover"]').popover()

    $('.carousel').carousel({
    interval: 5000
    });

    $('#exampleModal').on('show.bs.modal',function(e){
        console.log("El modal se está comenzando a mostrar.");

        //Cambia el color del botón y se deshabilita
        $("#btnContacto").removeClass('btn-outline-success');
        $("#btnContacto").addClass('btn-primary');
        $("#btnContacto").prop('disabled', true);
    });
    
    $('#exampleModal').on('shown.bs.modal',function(e){
    console.log("El modal se está mostrando.");
    });

    $("#exampleModal").on('hide.bs.modal',function(e){
    console.log("El modal se está comenzando a ocultar.");
    });

    $("#exampleModal").on('hidden.bs.modal',function(e){
        console.log("El modal se ha ocultado.");
        //El botón recupera su color original y es habilitado nuevamente
        $("#btnContacto").removeClass('btn-primary');
        $("#btnContacto").addClass('btn-outline-success');
        $("#btnContacto").prop('disabled', false);
    });
});