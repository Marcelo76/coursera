module.exports = function(grunt){

    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });

    grunt.initConfig({
        sass:{
            dist:{
                files:[{
                    expand: true,
                    cwd: 'css',
                    src: ['css/*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },

        watch:{
            files:['css/*.scss'],
            task: ['css']
        },

        browserSync:{
            dev: {
                bsFiles: { //browser files
                    src: [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: './'   //Directorio base para nuestro servidor
                    }
                }
            }
            
        },

        //Compilar y comprimir imágenes
        imagemin: {
            dynamic:{
                files: [{
                    expand: true,
                    cwd: './',
                    src: 'images/*.{png,gif,jpg,jpeg}',
                    dest: 'dist/'
                }]
            }
        },

        //Copiar los archivos html desde el source a la carpeta dist
        copy: {
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './',
                    src: ['*.html', '!*.min.html'],
                    dest: 'dist'
                }]
            },
            fonts: {
                files: [{
                    expand: true, 
                    dot: true, 
                    cwd: 'node_modules/open_iconic/font',
                    src: ['fonts/*.*'],
                    dest: 'dist'
                }]
            }
        },

        //Eliminar carpeta dist para volver a crearla
        clean: {
            build: {
                src: ['dist/']
            }
        },

        //minifica los archiuvos css y los copia
        cssmin: {
            target: {
                files: [{
                  expand: true,
                  cwd: 'css',
                  src: [
                        '*.css',
                        'node_modules/bootstrap/dist/css/bootstrap.css',
                        'node_modules/open-iconic/font/css/open-iconic-bootstrap.css',
                        '!*.min.css'
                    ],
                  dest: 'dist/css',
                  ext: '.css'
                }]
              }
        },

        //minifica los archiuvos js y los copia 
        uglify:{
            my_target: {
                files: {
                  'dist/js/index.js': [
                      'js/*.js', 
                      'node_modules/jquery/dist/jquery.slim.min.js',
                      'node_modules/popper.js/dist/umd/popper.min.js',
                      'node_modules/bootstrap/dist/js/bootstrap.min.js'
                    ]
                }
              }
        },

        //Evita que los archivos listados no sean cacheables (El usuario no necesitariía borar cache para observar cambios)
        filerev: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            },

            release: {
                files: [{
                    src: [
                        'dist/js/*.js',
                        'dist/css/*.css',
                    ]
                }]
            }
        },

        concat: {
            options: {
                separator: ';'
            },
            dist: {}
        },

        //minifica y aplica uglify a los archivos html listados (se excluye index.html por encontrarse en otro directorio)
        useminPrepare: {
            foo: {
                dest: 'dist',                
                src: ['index.html', 'about.html', 'precios.html', 'contacto.html']
            },
            options:{
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function(context, block){
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0,
                                    rebase: false
                                }
                            }
                        }]
                    }
                }
            }
        },

        //
        usemin: {
            html: ['dist/index.html', 'dist/about.html', 'dist/precios.html', 'dist/contacto.html'],
            options: {
                assetsDir: ['dist','dist/css','dist/js']
            }
        },
        
    });

    //grunt.loadNpmTasks('grunt-contrib-sass');
    //grunt.loadNpmTasks('grunt-contrib-watch');    
    //grunt.loadNpmTasks('grunt-browser-sync');
    //grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.registerTask('css',['sass']);
    grunt.registerTask('default',['browserSync','watch']);    
    grunt.registerTask('img:compress',['imagemin']);
    grunt.registerTask('build',[
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',        
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'
    ]);
}